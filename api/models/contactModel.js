const mongoose = require('mongoose');

const ContactScehma = mongoose.Schema({

    // must be assocaite the contant with the user_id from User model 
    // add documnet made of user id 
    user_id:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
        ref:"User"
     },

    name: {
    type: String,
        required: [true, "Please add the contact"],
    },
    email: {
        type:String,
        required: [true, "Please add the email"]
    },
    phone: {
        type:String,
        required: [true, "Please add the phone number"]
    }
},
    {
        timestamps: true
    }
)


module.exports = mongoose.model("Contact", ContactScehma)