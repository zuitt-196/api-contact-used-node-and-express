const express = require('express');
const connectDb = require('./config/dbConnection');
const errorhandler = require('./middleware/errorHandler');
const dotenv  = require('dotenv').config();
const app = express();
const contactRoutes = require('./routes/contactRoutes')
const userrRoutes = require('./routes/userrRoutes')

// CONNECTION DATABSE
connectDb();

// [MIDDLEWARE SECTION] is use for request the body
app.use(express.json());




// [ROUTER SECTION] 
app.use("/api/contacts",contactRoutes)
app.use("/api/user", userrRoutes)

app.use(errorhandler); // must be put adter the api router execute 





// [THE SEVER NOTICAATION SECTION]
const PORT = process.env.PORT || 5000 ;
app.listen(PORT , () => {
    console.log(`Server running on port ${PORT}`)
})