const asyncHandler = require('express-async-handler');
const JWT = require('jsonwebtoken');


const validateToken = asyncHandler(async(req, res, next) => {
    // console.log(req.headers)
    let token;
    let authHeader = await req.headers.Authorization || req.headers.authorization;
    if (authHeader && authHeader.startsWith("Bearer")) {
        token = authHeader.split(" ")[1];
        console.log("token:", token)
        console.log("process.env.ACCES_TOKEN_SECRET:",process.env.ACCES_TOKEN_SECRET);
        // VERIFE
        JWT.verify(token, process.env.ACCES_TOKEN_SECRET, (err,decode) =>{
                console.log(err);
                if (err) {
                    res.status(401);
                    throw new Error("User is not Authorize");
                };

                // console.log(decode.user);
                // store it into the req.userD use the req.user property
                req.user = decode.user;
                next();
        });                                   
    };

    if(!token){
        res.status(401);
        throw new Error("User is not Authorize or token is missing");
    };       
});

module.exports = validateToken;

