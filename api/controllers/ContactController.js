const asyncHandler = require('express-async-handler');
const Contact  = require('../models/contactModel')

// desc GET all constact 
// route GET /api/contact
// access public chanage into private since we used  toke validation
const getContact = asyncHandler (async (req, res) =>{
    const contacts =  await Contact.find({user_id:req.user._id});
     res.status(200).json({
            message:contacts
     });
})

// desc POST all constact 
// route POST /api/contact
// access public chanage into private since we used  toke validation
const createContact = asyncHandler(async (req, res) =>{
    const {name, email, phone} = await req.body;
    if (!name || !email || !phone) {
        res.status(400);
        throw new Error("All fields is mandatory");
        } 
    const contact = await Contact.create({
            name,
            email,
            phone,
            user_id: req.user_id
    });
    res.status(201).json({
           message: contact
    });
})


// desc GET  BY ID constact 
// route GET BY ID  /api/contact/:id
// access public chanage into private since we used  toke validation
const gettContactByid= asyncHandler(async(req, res) =>{
    const contact = await Contact.findById(req.params.id);
    if (!contact) {
            res.status(404);
            throw new Error("Contact not found");
        
    }

    res.status(200).json({
           message:contact
    });
})

// desc UPDATE  BY ID constact 
// route UPDATE BY ID  /api/contact/:id
// access public chanage into private since we used  toke validation
const updateContactByid= asyncHandler(async(req, res) =>{
    const contact = await Contact.findById(req.params.id);

    if (!contact) {
        res.status(404);
        throw new Error("Contact not found");
    
}
    const updateContact = await Contact.findByIdAndUpdate(
            req.params.id, 
            req.body,
            {new:true}
    )
    res.status(200).json({
           message:updateContact
    });
})

// desc DELETE  BY ID constact 
// route DELETE BY ID  /api/contact/:id
// access public chanage into private since we used  toke validation
const deleteContactByid= asyncHandler(async (req, res) =>{
    const contact = await Contact.findById(req.params.id);
    if (!contact) {
        res.status(404);
       throw new Error("Contact not found");
    
}
    await Contact.deleteOne();
    res.status(200).json(contact);
})

// EXPORTS 
module.exports ={
    getContact,
    createContact,
    gettContactByid,
    updateContactByid,
    deleteContactByid
}