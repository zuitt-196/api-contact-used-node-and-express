const asyncHandler = require('express-async-handler');
const User = require('../models/UserModel');
const bcrypt = require('bcrypt');
const JWT = require('jsonwebtoken');



// desc POST all user
// route POST /api/ser
// access public
const regiserUser = asyncHandler (async (req, res) =>{
//    ditructue the object from req body
      const {username , email, password} = req.body;

    // ERROR HANDLER
    if (!username || !password || !email) {
        res.status(400);
            throw new Error("All fields are mandatory!")
        
    }
    // FIND THE EMAIL OBJECR  IS EXISTED 
    const userAvailable = await User.findOne({email});
    if (userAvailable) {
        res.status(400);
        throw new Error("User already Register!");
    }
    // HASHES PASSWORD
    const hashePassword = await bcrypt.hash(password, 10);

    //CREATE THE USER 
    const user = await User.create({
        username,
        email,
        password:hashePassword
    })
    
    if (user) {
        res.status(201).json({_id: user._id, email: user.email})
    }else{
        res.status(400)
        throw new Error("User data is not valid");
    }
        
})


// desc POST all user
// route POST /api/user/login
// access public
const loginUser = asyncHandler (async (req, res) =>{
    // DESFRACTURE THHE Req.body 
    const {email, password} = req.body;

     // ERROR HANDLER
     if (!email || !password) {
         res.status(400);
         throw new Error("ALl fields madatory!");
    }
    //  find the user emailin the database
     const user = await User.findOne({email});


    //  compare password with hashepassword;
    if(user && (await bcrypt.compare(password, user.password))){
        // CREATE ACCESA TOKEN
        const accesToken = await JWT.sign({
                user:{
                    username: user.username,
                    email: user.email,
                    _id:user._id
                }
        }, process.env.ACCES_TOKEN_SECRET,{
            expiresIn: "10m"
        });
        
             res.status(201).json({accesToken});
    }else{
         res.status(401);
        throw new Error("email or password is incorrect")
    }
})


// desc Current user info
// route POST /api/user/current
// access private
// mostly it used middleware validation ti validate the token or only authication able to access
const currentUser = asyncHandler (async (req, res) => {
    // res.send("current user informatiom ")
    res.json(req.user);
})


module.exports = {
    regiserUser,
    loginUser,
    currentUser
}