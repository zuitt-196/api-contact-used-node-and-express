const mongoose = require('mongoose');

const connectDb = async () =>{
    try {
        const conneect = await mongoose.connect(process.env.CONNECT_STRING);
        console.log("Database connected:", conneect.connection.host, conneect.connection.name);
    } catch (error) {
        console.log(error);
        process.exit(1); 
    }
}

module.exports =connectDb;