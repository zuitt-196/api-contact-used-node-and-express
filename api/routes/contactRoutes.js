const router = require('express').Router();

// IMPORT CONTROLLER
const {getContact, createContact, gettContactByid,updateContactByid,deleteContactByid} = require('../controllers/ContactController');
const validateToken = require('../middleware/validate');


// add middleware validation
// router.use(validateToken);
// GET ROUTE METHOD
router.get("/getContact",getContact);
// POST ROUTE METHOD
router.post("/postContact",validateToken, createContact);
// GET BY ID ROUTE METHOD
router.get("/getContactById/:id", gettContactByid);
// PUT BY ID ROUTE METHOD
router.put("/updateContacById/:id",updateContactByid);
// DELETE  BY ID ROUTE METHOD
router.delete("/deleteContacByid/:id",deleteContactByid);







module.exports = router;

