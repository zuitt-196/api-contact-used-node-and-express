const { regiserUser, loginUser,currentUser } = require('../controllers/UserController');
const validateToken = require('../middleware/validate');

const router = require('express').Router();

// LOG IN ROUTE API ENDPOINT
router.post('/register', regiserUser);
router.post('/login', loginUser);
router.post('/current',validateToken, currentUser)





module.exports = router;

